import sys
import os
import math
import argparse
import pygame
import numpy
import shutil
import graph_tool
import graph_tool.all as gta
import random
from pprint import pprint


class Environment:
    """
    :type characteristic_conf_list:list[(Configuration,Configuration)]
    :type conf_space_torus:list[Torus]
    """

    def __init__(self):

        self.peg_color = (180, 0, 30)
        self.peg_width = 7

        self.rod_length = 70
        self.rod_color = (0, 0, 255)
        self.rod_joint_width = 5
        self.rod_width = 4

        self.obstacle_color = (0, 0, 0)
        self.free_color = (255, 255, 255)

        self.sample_color = (255, 0, 0)

        self.separator_color = (127, 30, 200)
        self.separator_size = 5

        self.resolution = math.pi / 60

        self.torus_main_radius = 400
        self.torus_sub_radius = 200

        self.digitized_angles = numpy.linspace(0, 2 * math.pi - self.resolution, (2 * math.pi / self.resolution))

        self.workspace_img = None
        self.workspace_size = None
        self.peg_locations = None

        self.characteristic_conf_list = None

        self.conf_spaces = None
        self.conf_space_size = None
        self.conf_space_volume = None
        self.conf_space_len = None

        self.conf_spaces_occupied_set = None
        self.conf_spaces_free_set = None

        self.conf_space_coordinate = []

    def load_environment(self):
        print("Loading environment")
        self.workspace_img = pygame.image.load("instance/workspace.png")
        self.workspace_size = self.workspace_img.get_size()

        # Read peg locations
        self.peg_locations = []
        if os.path.exists("instance/peg_locations.txt"):
            print("Reading peg locations")
            peg_file = open("instance/peg_locations.txt", 'r')
            self.peg_locations = []
            for peg in peg_file:
                self.peg_locations.append(tuple([int(p) for p in peg.split(",")]))
            peg_file.close()

        user_input = input("Would you like to add: new pegs [y/n]:")
        modify_pegs = 'y' in user_input.lower()
        user_input = input("Would you like to see system [y/n]:")
        see_system = 'y' in user_input.lower()

        if modify_pegs or see_system:
            print("Generating peg locations")

            self.pygame_screen_size = self.workspace_size
            self.prep_screen()
            clock = pygame.time.Clock()

            print_workspace(self)
            pygame.display.flip()
            cont = True
            alpha_1 = 0
            alpha_1_change = 0
            alpha_2 = 0
            alpha_2_change = 0
            while cont:
                for event in pygame.event.get():
                    if event.type == pygame.QUIT:
                        sys.exit()

                    elif event.type == pygame.KEYDOWN:
                        if event.key == pygame.K_BACKSPACE or event.key == pygame.K_DELETE:
                            self.peg_locations = self.peg_locations[0:-1]
                        elif event.key == pygame.K_ESCAPE:
                            sys.exit()
                        elif event.key == pygame.K_SPACE:
                            cont = False
                        elif event.key == pygame.K_UP:
                            alpha_2_change = math.pi / 90
                        elif event.key == pygame.K_DOWN:
                            alpha_2_change = -math.pi / 90
                        elif event.key == pygame.K_RIGHT:
                            alpha_1_change = math.pi / 90
                        elif event.key == pygame.K_LEFT:
                            alpha_1_change = -math.pi / 90

                    elif event.type == pygame.KEYUP:
                        if event.key == pygame.K_UP or event.key == pygame.K_DOWN:
                            alpha_2_change = 0
                        elif event.key == pygame.K_RIGHT or event.key == pygame.K_LEFT:
                            alpha_1_change = 0

                    elif modify_pegs and event.type == pygame.MOUSEBUTTONUP:
                        mouse_pos = pygame.mouse.get_pos()
                        self.peg_locations.append(mouse_pos)
                        print(mouse_pos)

                alpha_1 += alpha_1_change
                alpha_2 += alpha_2_change

                print_workspace(self)

                for peg_id in range(len(self.peg_locations)):
                    print_rod(self, Configuration(peg_id, alpha_1 + (math.pi * 0.1 * peg_id), alpha_2, None), False, (50, 100 + (peg_id * 10), 100 - (peg_id * 10)))

                pygame.display.flip()

                clock.tick(30.0)

            if modify_pegs:
                print("Saving new pegs to peg file")
                try:
                    os.remove("instance/peg_locations.txt")
                except:
                    pass
                peg_file = open("instance/peg_locations.txt", 'w')

                try:
                    shutil.rmtree("conf")
                except:
                    pass

                for p in self.peg_locations:
                    peg_str = ",".join([str(i) for i in p])
                    peg_file.write(peg_str + "\n")
                peg_file.close()

        self.characteristic_conf_list = self.find_characteristic_configurations()

        self.generate_configuration_spaces()
        self.conf_space_size = self.conf_spaces[0].get_size()
        self.conf_space_volume = self.conf_space_size[0] * self.conf_space_size[1]
        self.conf_space_len = len(self.conf_spaces)

        self.conf_space_coordinate = []

        for index in range(len(self.conf_spaces)):
            self.conf_space_coordinate.append((self.workspace_size[0] + self.separator_size, index * (self.conf_space_size[1] + self.separator_size)))

        self.pygame_screen_size = (self.workspace_size[0] + self.conf_space_size[0], max(self.workspace_size[1], self.conf_space_size[1] * self.conf_space_len))

    def find_characteristic_configurations(self):
        """

        :rtype:list[(Configuration,Configuration)]
        :type self:Environment
        :param self:
        :return:
        """

        all_conf = []

        import itertools
        l = self.rod_length
        for pair in itertools.combinations(enumerate(self.peg_locations), 2):

            relative_end_point_0 = tuple(numpy.subtract(pair[1][1], pair[0][1]))
            x0 = relative_end_point_0[0]
            y0 = relative_end_point_0[1]

            relative_end_point_1 = tuple(numpy.subtract(pair[0][1], pair[1][1]))
            x1 = relative_end_point_1[0]
            y1 = relative_end_point_1[1]

            # if rod's length is enough to reach
            distance = numpy.sqrt(numpy.sum(numpy.square(relative_end_point_0)))
            if distance <= self.rod_length * 2:
                # attached to pair[0]
                p01 = math.sqrt(1 - ((x0 ** 2 + y0 ** 2 - l ** 2 - l ** 2) / (2 * l * l)) ** 2)
                p02 = (x0 ** 2 + y0 ** 2 - l ** 2 - l ** 2) / (2 * l * l)

                # attached to pair[1]
                p11 = math.sqrt(1 - ((x1 ** 2 + y1 ** 2 - l ** 2 - l ** 2) / (2 * l * l)) ** 2)
                p12 = (x1 ** 2 + y1 ** 2 - l ** 2 - l ** 2) / (2 * l * l)

                # -----

                # attached to pair[0]
                angle_02 = numpy.arctan2(p01, p02)
                k01 = l + l * math.cos(angle_02)
                k02 = l * math.sin(angle_02)
                angle_01 = numpy.arctan2(y0, x0) - numpy.arctan2(k02, k01)

                angle_01 = (angle_01 + (2 * math.pi)) % (2 * math.pi)
                angle_02 = (angle_02 + (2 * math.pi)) % (2 * math.pi)

                ang_01_coordinate = numpy.digitize(angle_01, self.digitized_angles) - 1
                ang_02_coordinate = numpy.digitize(angle_02, self.digitized_angles) - 1

                conf_00 = Configuration(pair[0][0], self.digitized_angles[ang_01_coordinate], self.digitized_angles[ang_02_coordinate], (ang_01_coordinate, ang_02_coordinate))

                # attached to pair[1]
                angle_12 = numpy.arctan2(p11, p12)
                k11 = l + l * math.cos(angle_12)
                k12 = l * math.sin(angle_12)
                angle_11 = numpy.arctan2(y1, x1) - numpy.arctan2(k12, k11)

                angle_11 = (angle_11 + (2 * math.pi)) % (2 * math.pi)
                angle_12 = (angle_12 + (2 * math.pi)) % (2 * math.pi)

                ang_11_coordinate = numpy.digitize(angle_11, self.digitized_angles) - 1
                ang_12_coordinate = numpy.digitize(angle_12, self.digitized_angles) - 1

                conf_10 = Configuration(pair[1][0], self.digitized_angles[ang_11_coordinate], self.digitized_angles[ang_12_coordinate], (ang_11_coordinate, ang_12_coordinate))

                # -----------------------------------
                # When -p01 is minus

                # attached to pair[0]
                angle_02 = numpy.arctan2(-p01, p02)
                k01 = l + l * math.cos(angle_02)
                k02 = l * math.sin(angle_02)
                angle_01 = numpy.arctan2(y0, x0) - numpy.arctan2(k02, k01)

                angle_01 = (angle_01 + (2 * math.pi)) % (2 * math.pi)
                angle_02 = (angle_02 + (2 * math.pi)) % (2 * math.pi)

                ang_01_coordinate = numpy.digitize(angle_01, self.digitized_angles) - 1
                ang_02_coordinate = numpy.digitize(angle_02, self.digitized_angles) - 1

                conf_01 = Configuration(pair[0][0], self.digitized_angles[ang_01_coordinate], self.digitized_angles[ang_02_coordinate], (ang_01_coordinate, ang_02_coordinate))

                # attached to pair[1]
                angle_12 = numpy.arctan2(-p11, p12)
                k11 = l + l * math.cos(angle_12)
                k12 = l * math.sin(angle_12)
                angle_11 = numpy.arctan2(y1, x1) - numpy.arctan2(k12, k11)

                angle_11 = (angle_11 + (2 * math.pi)) % (2 * math.pi)
                angle_12 = (angle_12 + (2 * math.pi)) % (2 * math.pi)

                ang_11_coordinate = numpy.digitize(angle_11, self.digitized_angles) - 1
                ang_12_coordinate = numpy.digitize(angle_12, self.digitized_angles) - 1

                conf_11 = Configuration(pair[1][0], self.digitized_angles[ang_11_coordinate], self.digitized_angles[ang_12_coordinate], (ang_11_coordinate, ang_12_coordinate))

                all_conf.append((conf_00, conf_11))
                all_conf.append((conf_01, conf_10))

        return all_conf

    def prep_screen(self):
        self.pygame_screen = pygame.display.set_mode(self.pygame_screen_size)

    def angle_to_conf_space_coordinate(self, alpha_1, alpha_2):
        return (numpy.digitize(alpha_1, self.digitized_angles) - 1, numpy.digitize(alpha_2, self.digitized_angles) - 1)

    def check_conf_space_free(self, id, loc):
        if self.conf_spaces[id].get_at(loc) == self.obstacle_color:
            return False
        else:
            return True

    def generate_configuration_spaces(self):
        """
        :type self:Environment
        :param self:
        :return:
        """

        self.conf_spaces = [None] * len(self.peg_locations)
        self.conf_spaces_occupied_set = [None] * len(self.peg_locations)
        self.conf_spaces_free_set = [None] * len(self.peg_locations)

        if os.path.exists("conf"):
            print("Reading configuration space from conf folder")
            for filename in os.listdir(os.getcwd() + "/conf"):
                if ".png" in filename:
                    id = int(filename.split(sep=".")[-2])
                    self.conf_spaces[id] = pygame.image.load("conf/" + filename)

        else:
            print("Generating Configurations")
            os.mkdir("conf")
            if False:
                from multiprocessing import Process
                process_list = []
                for peg_id in range(len(self.peg_locations)):
                    p = Process(target=self.try_all_configurations, args=(peg_id))
                    process_list.append(p)
                    p.start()

                for each_process in process_list:
                    each_process.join()
            else:
                for peg_id in range(len(self.peg_locations)):
                    conf_space = self.try_all_configurations(peg_id)
                    self.conf_spaces[peg_id] = conf_space

        for id in range(len(self.peg_locations)):
            occupied_set = set()
            free_set = set()

            size = self.conf_spaces[id].get_size()
            for i in range(size[0]):
                for j in range(size[1]):
                    if self.conf_spaces[id].get_at((i, j)) == self.obstacle_color:
                        occupied_set.add((i, j))
                    else:
                        free_set.add((i, j))

            self.conf_spaces_occupied_set[id] = occupied_set
            self.conf_spaces_free_set[id] = free_set

    def try_all_configurations(self, peg_id):
        """
        :type self:Environment
        :type peg_id:int

        :param self:
        :param peg_id:
        :return:
        """
        print("will try all configurations to generate configuration space, peg_id: " + str(peg_id))
        conf_space_size = len(self.digitized_angles)
        cur_conf = Configuration

        conf_space = pygame.Surface((conf_space_size, conf_space_size))
        conf_space.fill(self.free_color)

        for alpha_1_id in range(len(self.digitized_angles)):
            for alpha_2_id in range(len(self.digitized_angles)):
                cur_conf.alpha_1 = self.digitized_angles[alpha_1_id]
                cur_conf.alpha_2 = self.digitized_angles[alpha_2_id]

                rod_points_rel = rod_point_relative(self, cur_conf)

                rod_points_absolute = [tuple(i) for i in numpy.add(rod_points_rel, self.peg_locations[peg_id])]

                if self.linear_collision_check(self.workspace_img, rod_points_absolute):
                    conf_space.set_at((alpha_1_id, alpha_2_id), self.obstacle_color)

        pygame.image.save(conf_space, "conf/" + str(peg_id) + ".png")
        return conf_space

    def linear_collision_check(self, image, ipoints):
        """
        :type self:Environment
        :type points:List[float]

        :param self:
        :param points:
        :return:
        """

        image_size=image.get_size()
        for i, j in zip(ipoints[0:-1], ipoints[1:]):
            len = numpy.linalg.norm(numpy.subtract(i, j))
            pl0 = [int(i) for i in numpy.linspace(i[0], j[0], int(len + 1))]
            pl1 = [int(i) for i in numpy.linspace(i[1], j[1], int(len + 1))]
            pl = [tuple(i) for i in zip(pl0, pl1)]

            for i in pl:
                if i[0]>=image_size[0] or i[1]>=image_size[1] or image.get_at(i) == self.obstacle_color:
                    return True  # there is collision
        return False


class PRM:
    """
    :type graph:graph_tool.Graph
    """

    def __init__(self, env, connect_distance, sample_size):
        """
        :type env:Environment
        :param env:
        """
        self.samples = None
        self.connect_distance = connect_distance
        self.connect_distance_angle = 2 * math.pi / 180
        self.sample_size = sample_size
        self.env = env
        self.graph = None
        pass

    def sample_conf_spaces(self):
        """
        :type env:Environment
        :param env:
        :return:
        """

        # self.samples=[None]*len(env.conf_spaces)
        print("PRM: sampling configuration space")

        self.samples = []
        index = 0

        for conf_space in self.env.conf_spaces:

            samples_normalized = numpy.random.random((self.sample_size, 2))
            sample_list = [tuple(numpy.subtract(i, (1, 1))) for i in numpy.digitize(numpy.multiply(samples_normalized, math.pi * 2), self.env.digitized_angles)]

            char_conf = [(i.alpha_1, i.alpha_2) for i, j in self.env.characteristic_conf_list if index == i.peg_id]
            char_conf += [(j.alpha_1, j.alpha_2) for i, j in self.env.characteristic_conf_list if index == j.peg_id]
            sample_list += [tuple(numpy.subtract(i, (1, 1))) for i in numpy.digitize(char_conf, self.env.digitized_angles)]

            sample_list = list(set(sample_list))

            clear_sample_list = []
            for each_sample in sample_list:
                if conf_space.get_at(each_sample) == self.env.free_color:
                    clear_sample_list.append(each_sample)
            self.samples.append(clear_sample_list)

            index += 1

    def sample_conf_spaces_2(self):
        """
        :type env:Environment
        :param env:
        :return:
        """

        # self.samples=[None]*len(env.conf_spaces)
        print("PRM: sampling configuration space")

        self.samples = []
        index = 0

        occupied_set = set()
        free_set = set()

        for conf_space in self.env.conf_spaces:

            sampled_space = set()

            size = conf_space.get_size()
            for i in range(size[0]):
                for j in range(size[1]):
                    if conf_space.get_at((i, j)) == self.env.obstacle_color:
                        occupied_set.add((i, j))
                    else:
                        free_set.add((i, j))

            char_conf = [(i.alpha_1, i.alpha_2) for i, j in self.env.characteristic_conf_list if index == i.peg_id]
            char_conf += [(j.alpha_1, j.alpha_2) for i, j in self.env.characteristic_conf_list if index == j.peg_id]
            char_conf_locs = [tuple(numpy.subtract(i, (1, 1))) for i in numpy.digitize(char_conf, self.env.digitized_angles)]

            sampled_space |= (set(char_conf_locs) & free_set)
            all_members_set = free_set | occupied_set
            sampled_space |= set(random.sample(all_members_set, self.sample_size))
            sampled_space -= occupied_set

            while len(sampled_space) < self.sample_size:
                member = random.sample(occupied_set, 1)[0]
                new_samples = set([tuple(i) for i in numpy.random.multivariate_normal(member, [[119, 0], [0, 119]], (self.sample_size - len(sampled_space)) * 2).astype('int')])
                sampled_space |= (new_samples & free_set)

            self.samples.append(list(sampled_space))

    def global_to_local_loc(self, global_loc):
        real_pos_x = (global_loc[0] % (2 * self.env.conf_space_size[0]))
        real_pos_y = (global_loc[1] % (2 * self.env.conf_space_size[1]))

        cur_peg_id = int(global_loc[0] / (2 * self.env.conf_torus_dims[0]))
        return (cur_peg_id, (real_pos_x, real_pos_y))

    def local_to_global_loc(self, local_location, peg_id):
        return tuple(numpy.add(local_location, numpy.multiply(self.env.conf_space_size, 2 * peg_id)))

    def all_neigbours_with_dist(self, loc, dist):
        neigbours = []
        dist = int(math.ceil(dist))
        for i in range(-dist, dist):
            for j in range(-dist, dist):
                ni = (i + loc[0] + self.env.conf_space_size[0]) % self.env.conf_space_size[0]
                nj = (j + loc[1] + self.env.conf_space_size[1]) % self.env.conf_space_size[1]
                neigbours.append((ni, nj))

        return neigbours

    def generate_graph(self):
        """
        :type env:Environment
        :param env:
        :return:
        """
        if not os.path.exists("conf/my_graph"):

            print("PRM: generating a new graph")

            self.sample_conf_spaces()

            # Assign vertex properties
            g = graph_tool.Graph(directed=False)

            char_con = dict()
            vertex_conf = g.new_vertex_property("python::object")
            pos = g.new_vertex_property("vector<double>")
            cost = g.new_edge_property("float")

            for peg_id in range(len(self.env.peg_locations)):
                print("PRM: Processing peg:" + str(peg_id))
                cur_vertices = dict()
                for cur_sample_loc in self.samples[peg_id]:
                    v = g.add_vertex()

                    # cur_pos = self.local_to_global_loc(self.env.conf_space_torus[peg_id].get_loc(*i), peg_id)
                    pos[v] = self.local_to_global_loc(cur_sample_loc, peg_id)
                    cur_conf = Configuration(peg_id, self.env.digitized_angles[cur_sample_loc[0]], self.env.digitized_angles[cur_sample_loc[1]], cur_sample_loc)
                    vertex_conf[v] = cur_conf

                    # -------------------------------------------------
                    # Check if this is characteristic sample
                    characteristics = [(i, j) for i, j in self.env.characteristic_conf_list if i.peg_id == peg_id and i.conf_space_loc == cur_sample_loc]
                    if len(characteristics) > 0:
                        if characteristics[0] in char_con.keys():
                            char_con[characteristics[0]][0] = v
                        else:
                            char_con[characteristics[0]] = [v, None]

                    # Check if this is characteristic sample
                    characteristics = [(i, j) for i, j in self.env.characteristic_conf_list if j.peg_id == peg_id and j.conf_space_loc == cur_sample_loc]
                    if len(characteristics) > 0:
                        if characteristics[0] in char_con.keys():
                            char_con[characteristics[0]][1] = v
                        else:
                            char_con[characteristics[0]] = [None, v]

                    for nv_loc, nv in cur_vertices.items():
                        (vec, leng) = self.get_angle_vector(cur_sample_loc, nv_loc)
                        if self.connect_distance > leng and not self.angular_collision_check(peg_id, [cur_sample_loc, nv_loc]):
                            e = g.add_edge(v, nv)
                            cost[e] = leng

                    cur_vertices[cur_sample_loc] = v

            # -------------------------------------------------
            # Connect Characteristic samples
            for conf, vs in char_con.items():
                if vs[0] is None or vs[1] is None:
                    pass
                else:
                    e = g.add_edge(vs[0], vs[1])
                    cost[e] = 0

            g.vertex_properties['conf'] = vertex_conf
            g.vertex_properties['pos'] = pos
            g.edge_properties['cost'] = cost

            print("PRM: Saving new graph to configuration folder")
            g.save("conf/my_graph", fmt="gt")

            self.graph = g

        else:
            print("PRM: graph found in configuration folder so loading it")

            self.graph = graph_tool.Graph()
            self.graph.load("conf/my_graph", "gt")

            # gta.graph_draw(self.graph, pos=self.graph.vertex_properties['pos'], output_size=(1500, 1500))

    def generate_graph_2(self):
        """
        :type env:Environment
        :param env:
        :return:
        """
        if not os.path.exists("conf/my_graph"):

            print("PRM: generating a new graph")

            self.sample_conf_spaces()

            # Assign vertex properties
            g = graph_tool.Graph(directed=False)

            char_con = dict()
            vertex_conf = g.new_vertex_property("python::object")
            pos = g.new_vertex_property("vector<double>")
            cost = g.new_edge_property("float")

            for peg_id in range(len(self.env.peg_locations)):
                print("PRM: Processing peg:" + str(peg_id))

                cur_vertices = list()

                sample_size=len(self.samples[peg_id])
                sample_digitized = numpy.linspace(0, sample_size*8, 9)

                all_samples=[]
                for y in range(3):
                    for x in range(3):
                        all_samples += [(i[0] + x * self.env.conf_space_size[0], i[1]+ y * self.env.conf_space_size[1]) for i in self.samples[peg_id]]

                ts, tsp = gta.geometric_graph(all_samples, self.connect_distance)

                for si in range(len(self.samples[peg_id])):
                    cur_sample_loc = self.samples[peg_id][si]
                    v = g.add_vertex()

                    # cur_pos = self.local_to_global_loc(self.env.conf_space_torus[peg_id].get_loc(*i), peg_id)
                    pos[v] = self.local_to_global_loc(cur_sample_loc, peg_id)
                    cur_conf = Configuration(peg_id, self.env.digitized_angles[cur_sample_loc[0]], self.env.digitized_angles[cur_sample_loc[1]], cur_sample_loc)
                    vertex_conf[v] = cur_conf

                    # -------------------------------------------------
                    # Check if this is characteristic sample
                    characteristics = [(i, j) for i, j in self.env.characteristic_conf_list if i.peg_id == peg_id and i.conf_space_loc == cur_sample_loc]
                    if len(characteristics) > 0:
                        if characteristics[0] in char_con.keys():
                            char_con[characteristics[0]][0] = v
                        else:
                            char_con[characteristics[0]] = [v, None]

                    # Check if this is characteristic sample
                    characteristics = [(i, j) for i, j in self.env.characteristic_conf_list if j.peg_id == peg_id and j.conf_space_loc == cur_sample_loc]
                    if len(characteristics) > 0:
                        if characteristics[0] in char_con.keys():
                            char_con[characteristics[0]][1] = v
                        else:
                            char_con[characteristics[0]] = [None, v]

                    # for ng in set(list(ts.vertex(si).all_neighbours()) +list(tg.vertex(si).all_neighbours()) + list(trg.vertex(si).all_neighbours()) + list(tig.vertex(si).all_neighbours()) + list(tirg.vertex(si).all_neighbours())):
                    for ng in ts.vertex((4*int(sample_size))+si).all_neighbours():
                        real_ng=int(ng)-int(sample_digitized[(numpy.digitize(int(ng),sample_digitized)-1)])
                        if real_ng < len(cur_vertices):
                            nv = cur_vertices[real_ng]
                            nv_loc = self.samples[peg_id][real_ng]

                            if not self.angular_collision_check(peg_id, [cur_sample_loc, nv_loc]):
                                (vec, leng) = self.get_angle_vector(cur_sample_loc, nv_loc)
                                e = g.add_edge(v, nv)
                                cost[e] = leng

                    cur_vertices.append(v)

            # -------------------------------------------------
            # Connect Characteristic samples
            for conf, vs in char_con.items():
                if vs[0] is None or vs[1] is None:
                    pass
                else:
                    e = g.add_edge(vs[0], vs[1])
                    cost[e] = 0

            g.vertex_properties['conf'] = vertex_conf
            g.vertex_properties['pos'] = pos
            g.edge_properties['cost'] = cost

            gta.graph_draw(g, pos=pos)

            print("PRM: Saving new graph to configuration folder")
            g.save("conf/my_graph", fmt="gt")

            self.graph = g


        else:
            print("PRM: graph found in configuration folder so loading it")

            self.graph = graph_tool.Graph()
            self.graph.load("conf/my_graph", "gt")

    def find_path(self, init_conf, final_conf):
        """
        :type init_conf:Configuration
        :type final_conf:Configuration
        :param init_conf:
        :param final_conf:
        :return:
        """

        print("PRM: Finding a path.")

        init_conf.alpha_1 = (init_conf.alpha_1 + (2 * math.pi)) % (2 * math.pi)
        init_conf.alpha_2 = (init_conf.alpha_2 + (2 * math.pi)) % (2 * math.pi)
        final_conf.alpha_1 = (final_conf.alpha_1 + (2 * math.pi)) % (2 * math.pi)
        final_conf.alpha_2 = (final_conf.alpha_2 + (2 * math.pi)) % (2 * math.pi)

        # check if initial_conf and final_conf are feasable
        init_conf.conf_space_loc = self.env.angle_to_conf_space_coordinate(init_conf.alpha_1, init_conf.alpha_2)
        if not self.env.check_conf_space_free(init_conf.peg_id, init_conf.conf_space_loc):
            raise ValueError("Initial configuration space is not feasible")

        final_conf.conf_space_loc = self.env.angle_to_conf_space_coordinate(final_conf.alpha_1, final_conf.alpha_2)
        if not self.env.check_conf_space_free(final_conf.peg_id, final_conf.conf_space_loc):
            raise ValueError("Final configuration space is not feasible")

        g = self.graph.copy()

        # Find closest vertices
        init_global_loc = self.local_to_global_loc(init_conf.conf_space_loc, init_conf.peg_id)
        final_global_loc = self.local_to_global_loc(final_conf.conf_space_loc, final_conf.peg_id)

        init_low_loc = tuple([int(i) for i in numpy.subtract(init_global_loc, (self.connect_distance, self.connect_distance))])
        init_high_loc = tuple([int(i) for i in numpy.add(init_global_loc, (self.connect_distance, self.connect_distance))])

        final_low_loc = tuple([int(i) for i in numpy.subtract(final_global_loc, (self.connect_distance, self.connect_distance))])
        final_high_loc = tuple([int(i) for i in numpy.add(final_global_loc, (self.connect_distance, self.connect_distance))])

        init_connect_list = graph_tool.util.find_vertex_range(g, g.vertex_properties["pos"], [init_low_loc, init_high_loc])
        final_connect_list = graph_tool.util.find_vertex_range(g, g.vertex_properties["pos"], [final_low_loc, final_high_loc])

        # Add init and final nodes
        init_v = g.add_vertex()
        g.vertex_properties['conf'][init_v] = init_conf
        g.vertex_properties['pos'][init_v] = init_global_loc

        final_v = g.add_vertex()
        g.vertex_properties['conf'][final_v] = final_conf
        g.vertex_properties['pos'][final_v] = final_global_loc

        # Check if edges are free
        for v in init_connect_list:
            conf = g.vertex_properties['conf'][v]
            pos_cost = numpy.linalg.norm(numpy.subtract(init_conf.conf_space_loc, conf.conf_space_loc))
            if 0 < pos_cost <= self.connect_distance * 2:
                if not self.angular_collision_check(init_conf.peg_id, [init_conf.conf_space_loc, conf.conf_space_loc]):
                    e = g.add_edge(init_v, v)
                    g.edge_properties['cost'][e] = pos_cost

        for v in final_connect_list:
            conf = g.vertex_properties['conf'][v]
            pos_cost = numpy.linalg.norm(numpy.subtract(final_conf.conf_space_loc, conf.conf_space_loc))
            if 0 < pos_cost <= self.connect_distance * 2:
                if not self.angular_collision_check(final_conf.peg_id, [final_conf.conf_space_loc, conf.conf_space_loc]):
                    e = g.add_edge(final_v, v)
                    g.edge_properties['cost'][e] = pos_cost

        # gta.graph_draw(g, pos=g.vertex_properties['pos'], output_size=(1500, 1500))

        distance_map, pred_map = graph_tool.search.dijkstra_search(g, init_v, g.edge_properties['cost'])

        if distance_map[final_v] != float('inf'):
            cur_v = final_v
            path = []
            while True:
                if cur_v != init_v:
                    path.append(int(cur_v))
                    cur_v = pred_map[cur_v]
                else:
                    break
            path.append(int(init_v))
            path.reverse()

            conf_path = [g.vertex_properties['conf'][i] for i in path]
            return conf_path, path, g
        else:
            return None,None,g

    def angular_collision_check(self, conf_space_id, ipoints):
        """
            :type env:Environment
            :type points:List[float]

            :param env:
            :param points:
            :return:
            """

        image_size = self.env.conf_space_size
        for i, j in zip(ipoints[0:-1], ipoints[1:]):
            vec, leng = self.get_angle_vector(i, j)

            if(leng==0):
                pass

            cur_x=i[0]
            cur_y=i[1]
            inc_x=vec[0]/leng
            inc_y=vec[1]/leng

            pl=[(int(cur_x),int(cur_y))]
            while True:
                cur_x+=inc_x
                cur_y+=inc_y
                pl.append((int(cur_x), int(cur_y)))
                if (pl[-1][0]+image_size[0])%image_size[0]==j[0] and (pl[-1][1]+image_size[1])%image_size[1]==j[1]:
                    break


            collision = set(pl) & self.env.conf_spaces_occupied_set[conf_space_id]
            if len(collision) > 0:
                return True
        return False

    def get_angle_vector(self, point_1, point_2):
        mid = tuple([int(i) for i in numpy.divide(self.env.conf_space_size, 2)])
        shift = numpy.subtract(mid, point_1)
        shifted_point_2 = tuple([int(i) for i in numpy.mod(numpy.add(point_2, shift), self.env.conf_space_size)])
        vector = tuple(numpy.subtract(shifted_point_2, mid))
        len = numpy.linalg.norm(vector)

        return (vector, len)


class Configuration:
    def __init__(self, peg_id, alpha_1, alpha_2, conf_space_loc):
        self.peg_id = peg_id
        self.alpha_1 = alpha_1
        self.alpha_2 = alpha_2
        self.conf_space_loc = conf_space_loc

    def __repr__(self):
        width = 10
        swidth = str(width)
        st = "{:<" + swidth + "d}, {:<" + swidth + "f}, {:<" + swidth + "f}, {:<" + swidth + "}"
        st = st.format(self.peg_id, self.alpha_1, self.alpha_2, str(self.conf_space_loc))
        return st



def print_workspace(env):
    """
    :type env:Environment
    :param env:
    :return:
    """
    env.pygame_screen.blit(env.workspace_img, (0, 0))
    for peg_loc in env.peg_locations:
        pygame.draw.circle(env.pygame_screen, env.peg_color, peg_loc, env.peg_width)
        # env.pygame_screen.blit(env.peg_img, tuple([i - j for i, j in zip(peg_loc, env.peg_mid_point_offset)]))

    if env.conf_spaces is not None and len(env.conf_spaces) > 0:
        conf_space_size = env.conf_spaces[0].get_size()
        index = 0
        pygame.draw.line(env.pygame_screen, env.separator_color, (env.workspace_size[0] + math.floor(env.separator_size / 2), 0),
                         (env.workspace_size[0] + math.floor(env.separator_size / 2), env.pygame_screen_size[1]), env.separator_size)

        for conf_space in env.conf_spaces:
            env.pygame_screen.blit(conf_space, env.conf_space_coordinate[index])

            x = env.workspace_size[0] + math.floor(env.separator_size / 2)
            y = (index + 1) * (conf_space_size[1] + env.separator_size) - math.ceil(env.separator_size / 2)
            pygame.draw.line(env.pygame_screen, env.separator_color, (x, y), (env.pygame_screen_size[0], y), env.separator_size)

            index += 1


def print_samples(env, prm):
    """
    :type env:Environment
    :type prm:PRM
    :param env:
    :return:
    """
    if prm.samples is not None:
        for index in range(len(env.conf_spaces)):
            conf_space_image_loc = env.conf_space_coordinate[index]
            for sample in prm.samples[index]:
                loc = numpy.add(conf_space_image_loc, sample)
                pygame.draw.line(env.pygame_screen, env.sample_color, loc, loc, 1)


def print_rod(env, conf, rod_2_attached, color):
    """
    :type env:Environment
    :type conf:Configuration

    :param env:
    :param conf:
    :return:
    """
    peg_location = env.peg_locations[conf.peg_id]
    point_list = rod_point_relative(env, conf)

    absolute_point_list = [tuple(i.astype(int)) for i in numpy.add(point_list, peg_location)]
    pygame.draw.line(env.pygame_screen, color, absolute_point_list[0], absolute_point_list[1], env.rod_width)
    pygame.draw.circle(env.pygame_screen, color, absolute_point_list[0], env.rod_joint_width)

    pygame.draw.line(env.pygame_screen, color, absolute_point_list[1], absolute_point_list[2], env.rod_width)
    pygame.draw.circle(env.pygame_screen, color, absolute_point_list[1], env.rod_joint_width)

    if rod_2_attached:
        pygame.draw.circle(env.pygame_screen, color, absolute_point_list[2], env.rod_joint_width)


def rod_point_relative(env, conf):
    rod_1_endpoint = (int(env.rod_length * math.cos(conf.alpha_1)), int(env.rod_length * math.sin(conf.alpha_1)))

    rod_2_endpoint_relative = (int(env.rod_length * math.cos(conf.alpha_1 + conf.alpha_2)), int(env.rod_length * math.sin(conf.alpha_1 + conf.alpha_2)))
    rod_2_endpoint = tuple([i + j for i, j in zip(rod_1_endpoint, rod_2_endpoint_relative)])

    return [(0, 0), rod_1_endpoint, rod_2_endpoint]


def double_rod_point_relative(rod_points):
    new_list = []
    for i in range(len(rod_points) - 1):
        new_list.append(rod_points[i])
        new_list.append(tuple(numpy.rint(numpy.divide(numpy.add(rod_points[i], rod_points[i + 1]), 2)).astype(int)))
    new_list.append(rod_points[-1])
    return new_list


# def extend_path(path,sample_size):
#     """
#     :type path:list[Configuration]
#     :param path:
#     :return:
#     """
#     smooth_path=[]
#     for i,j in zip(path[0:-1],path[1:]):
#         pl0 = [i for i in numpy.linspace(i.alpha_1, j.alpha_1, sample_size)]
#         pl1 = [i for i in numpy.linspace(i.alpha_2, j.alpha_2, sample_size)]
#         for a0,a1 in zip(pl0,pl1):
#             smooth_path.append(Configuration(i.peg_id,a0,a1,None))
#
#     return smooth_path


def test_function():
    pygame.init()

    env = Environment()
    env.load_environment()

    sample_size = 1000
    prm = PRM(env, int(math.sqrt(env.conf_space_volume / sample_size)) * 3, sample_size)

    prm.generate_graph_2()

    clock = pygame.time.Clock()

    env.prep_screen()
    print_workspace(env)
    pygame.display.flip()

    init_conf = Configuration(0, 0, 0, env.angle_to_conf_space_coordinate(0, 0))
    # final_conf = Configuration(1, math.pi*0.90, math.pi*0.3, env.angle_to_conf_space_coordinate(0, 0))
    # final_conf = Configuration(1, math.pi * 0.5, math.pi, env.angle_to_conf_space_coordinate(0, 0))
    final_conf = Configuration(9, math.pi / 2, 0, env.angle_to_conf_space_coordinate(0, 0))
    conf_path, path, g = prm.find_path(init_conf, final_conf)

    # path=extend_path(path,60)


    # if path is None:
    #     gta.graph_draw(g, pos=g.vertex_properties['pos'])
    #     print("Solution not found!")
    # else:
    #
    #     v_col=g.new_vertex_property("vector<double>")
    #     e_col=g.new_edge_property("vector<double>")
    #     for v in path:
    #         v_col[v]=[0,0,1,1]
    #
    #     for s,d in zip(path[0:-1],path[1:]):
    #         e=g.edge(s,d)
    #         e_col[e]=[0,1,0,1]
    #
    #     gta.graph_draw(g, vertex_fill_color=v_col,edge_color=e_col, pos=g.vertex_properties['pos'])


    index = 0
    while True:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                sys.exit()
            elif event.type == pygame.MOUSEBUTTONUP:
                mouse_pos = pygame.mouse.get_pos()
                print(mouse_pos)

        print_workspace(env)
        print_samples(env, prm)
        print_rod(env, init_conf, False, (50, 255, 50))
        print_rod(env, final_conf, False, (255, 50, 50))

        if path is not None:
            print_rod(env, conf_path[index], False, (50, 50, 255))
            index += 1
            if index >= len(path):
                index = 0

        pygame.display.flip()
        clock.tick(10.0)


def main(argv):
    # parser = argparse.ArgumentParser()
    # parser.add_argument('-g', '--graph_desc_file', help="Graph Descriptive File", required=True, nargs=1)
    # args = parser.parse_args(argv)
    #
    # assert os.path.isfile(args.graph_desc_file[0])

    test_function()


if __name__ == '__main__':
    main(sys.argv[1:])
    # try:
    #     main(sys.argv[1:])
    # except Exception as e:
    #     import traceback,code
    #     type, value, tb = sys.exc_info()
    #     traceback.print_exc()
    #     last_frame = lambda tb=tb: last_frame(tb.tb_next) if tb.tb_next else tb
    #     frame = last_frame().tb_frame
    #     ns = dict(frame.f_globals)
    #     ns.update(frame.f_locals)
    #     code.interact(local=ns)
