This is an takehome exam question for motion planning. 
Task is given a 3 joint 2 limb monkey robot, reaching from a position to another without colliding with obstacles. The only way to move is holding pegs so problem gets a little bit harder.

It is implemented in python3. Pygame and graph_tool libraries are used.

Further details can be found at http://blog.ifyalciner.com/project/monkey_jungle/

Code may be a little bit messy since it was a very limited time project. but planning to fix that...

